//Use of double quotation marks is required in JSON Objects
//Syntax:
//{
// "propertyA": "valueA",
// "propertyB:" "valueB"
//}

// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// {
// 	"number": 1,
// 	"boolean": true
// }

//JavaScript Array of Objects:
let myArr = [
{name: "Jino"},
{name: "John"}
]

//Array of JSON Objects
// {
// 	"cities": [
// 		{"city": "Quezon City"},
// 		{"city": "Makati City"}
// 	]
// }

//Converting JS Data Into Stringified JSON

let batchesArr = [
	{batchname: 'Batch 145'},
	{batchname: 'Batch 146'},
	{batchname: 'Batch 147'}
]

let stringifiedData = JSON.stringify(batchesArr);
console.log(stringifiedData);

//Converting Stringified JSON into JavaScript Objects

let fixedData = JSON.parse(stringifiedData);
console.log(fixedData);